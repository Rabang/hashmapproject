package com.nextbank.map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class HashMapTest {

    @Test
    public void testHashMapPut() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("Diwali", "Day");
        assertEquals(1, hashMap.size());

        hashMap.put("Diwali", "Day");
        assertEquals(1, hashMap.size());

        hashMap.put("Hannukah", "Day");
        assertEquals(2, hashMap.size());

        hashMap.put(null, null);
        assertEquals(3, hashMap.size());
    }

    @Test
    public void testHashMapGet() {
        Map<String, String> hashMap = initializeHashMapForTesting();
        String christmas = hashMap.get("Merry");

        assertEquals("Christmas", christmas);

        String nullObject = hashMap.get(null);

        assertEquals(null, nullObject);
    }

    @Test
    public void testHashMapRemoveFirst() {
        Map<String, String> hashMap = initializeHashMapForTesting();
        assertEquals(4, hashMap.size());

        hashMap.remove("Valentines");

        assertEquals(3, hashMap.size());

        hashMap.remove(null);

        assertEquals(3, hashMap.size());
    }

    @Test
    public void testHashMapRemoveSecond() {
        Map<String, String> hashMap = initializeHashMapForTesting();
        assertEquals(4, hashMap.size());

        hashMap.remove("Valentines","Day");

        assertEquals(3, hashMap.size());

        hashMap.remove(null,null);

        assertEquals(3, hashMap.size());

    }

    @Test
    public void testHashMapSize() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Hello", "jolly");
        hashMap.put("Hello", "jolly");

    }

    private Map<String, String> initializeHashMapForTesting() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Merry", "Christmas");
        hashMap.put("Valentines", "Day");
        hashMap.put("Happy", "Easter");
        hashMap.put("Eid", "Mubarak");

        return hashMap;
    }

}
