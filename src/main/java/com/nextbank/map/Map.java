package com.nextbank.map;

public interface Map<K, V> {

    void put(K key, V value);
    V get(K key);
    boolean remove(K key);
    boolean remove(K key, V value);
    int size();
}
