package com.nextbank.map;

import com.nextbank.node.Node;

import java.util.*;

public class HashMap<K,V> implements Map<K, V> {

    List<Node<K, V>> nodes = new LinkedList<>();;

    @Override
    public void put(K key, V value) {
        if (Objects.isNull(get(key))) {
            Node node = new Node();
            node.setKey(key);
            node.setValue(value);
            nodes.add(node);
        }
    }

    @Override
    public V get(K key) {
        V result = null;
        for (Node<K, V> node : nodes) {
            if (node.getKey().equals(key)) {
                result = node.getValue();
            }
        }
        return result;
    }

    @Override
    public boolean remove(K key) {
        boolean isRemoved = false;

        Iterator iterator = nodes.iterator();
        while (iterator.hasNext()) {
            Node object = (Node) iterator.next();
            if (object.getKey().equals(key)) {
                iterator.remove();
                isRemoved = true;
                break;
            }
        }
        return isRemoved;
    }

    @Override
    public boolean remove(K key, V value) {
        boolean isRemoved = false;

        Iterator iterator = nodes.iterator();
        while (iterator.hasNext()) {
            Node object = (Node) iterator.next();
            if (object.getKey().equals(key) && object.getValue().equals(value)) {
                iterator.remove();
                isRemoved = true;
                break;
            }
        }

        for (Node<K, V> node : nodes) {
            if (node.getKey().equals(key) && node.getValue().equals(value)) {
                nodes.remove(node);
                isRemoved = true;
            }
        }
        return isRemoved;
    }

    @Override
    public int size() {
        return nodes.size();
    }

    //General validation
    // When you use (put) method null as a key
    // add remove method
    // Add unit test
    //Send to aaron.rasing@nextbank.ph
}
